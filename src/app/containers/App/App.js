import React from 'react';
import { Route, Switch, Link } from 'react-router-dom';

import { QuizGenerator, Home, Quiz } from 'containers';

const NavComp = () => (
    <React.Fragment>
        <p>
            <Link to="/">Home</Link>
        </p>
        <p>
            <Link to="/quiz">go solve test</Link>
        </p>
        <p>
            <Link to="/generator">create new</Link>
        </p>
    </React.Fragment>
);

export default () => (
    <div>
        <Route path="/(quiz|generator)" component={NavComp} />
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/quiz" component={Quiz} />
            <Route path="/generator" component={QuizGenerator} />
        </Switch>
    </div>
);
