import React from 'react';
import PropTypes from 'prop-types';

import { Header, QuizCodeForm } from 'components';

const Home = props => (
    <React.Fragment>
        <Header title="Feedback Matters" />
        <QuizCodeForm
            isLoading={props.isLoading}
            error={props.error}
            submitForm={props.getQuiz}
        />
    </React.Fragment>
);

Home.propTypes = {
    isLoading: PropTypes.bool,
    error: PropTypes.string,
    getQuiz: PropTypes.func,
};

Home.defaultProps = {
    isLoading: false,
    error: '',
    getQuiz: () => {},
};

export default Home;
