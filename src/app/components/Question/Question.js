import React from 'react';
import PropTypes from 'prop-types';

import IconButton from '@material-ui/core/IconButton';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MoreVert from '@material-ui/icons/MoreVert';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography/Typography';

import QUESTION_TYPE from 'constants/questionType';

import './question.scss';

class Question extends React.Component {
    state = {
        anchorEl: null,
    };

    getAnswerType() {
        const { type } = this.props.question;

        switch (type) {
        case QUESTION_TYPE.CHECK:
            return 'checkboxes';
        case QUESTION_TYPE.LONG:
            return 'long text';
        case QUESTION_TYPE.RADIO:
            return 'radio buttons';
        case QUESTION_TYPE.SHORT:
            return 'short text';
        default:
            return '';
        }
    }

    handleChange = (e) => {
        const text = e.target.value;

        this.props.changeQuestion({
            ...this.props.question,
            text,
        });
    };

    handleMenu = (e) => {
        this.setState({ anchorEl: e.currentTarget });
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    handleRemove = () => {
        const { id } = this.props.question;

        this.props.removeQuestion(id);
        this.handleClose();
    };

    render() {
        const { anchorEl } = this.state;
        const open = Boolean(anchorEl);

        return (
            <div className="question">
                <Typography variant="subtitle1" color="inherit" className="title">
                    Answer type:&nbsp;
                    {this.getAnswerType()}
                </Typography>
                <div className="menu">
                    <IconButton
                        aria-owns={open ? 'menu-question' : null}
                        aria-haspopup="true"
                        onClick={this.handleMenu}
                        color="inherit"
                    >
                        <MoreVert />
                    </IconButton>
                    <Menu
                        id="menu-question"
                        anchorEl={anchorEl}
                        anchorOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                        open={open}
                        onClose={this.handleClose}
                    >
                        <MenuItem onClick={this.handleRemove}>Remove Question</MenuItem>
                    </Menu>
                </div>
                <TextField
                    label="Question Text"
                    placeholder="Question Text"
                    fullWidth
                    margin="normal"
                    onChange={this.handleChange}
                />
            </div>
        );
    }
}

Question.propTypes = {
    question: PropTypes.shape({
        id: PropTypes.number.isRequired,
        type: PropTypes.string.isRequired,
    }).isRequired,
    changeQuestion: PropTypes.func.isRequired,
    removeQuestion: PropTypes.func.isRequired,
};

export default Question;
