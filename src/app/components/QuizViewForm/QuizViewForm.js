import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Button from '@material-ui/core/Button/Button';
import Typography from '@material-ui/core/Typography/Typography';

import { QuestionView } from 'components';

import './quizViewForm.scss';

class QuizViewForm extends React.Component {
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.handleSubmit();
    };

    render() {
        const {
            feedback,
            handleChange,
            isLoading,
            error,
            quiz,
        } = this.props;

        return (
            <form className="quiz-view-form" noValidate onSubmit={this.handleSubmit}>
                {error && (
                    <Typography className="description" variant="h5" color="error">
                        {error}
                    </Typography>
                )}
                <Typography className="description" variant="h5" color="inherit">
                    {quiz.description}
                </Typography>
                {quiz.questions && quiz.questions.map(q => (
                    <QuestionView
                        key={q.id}
                        data={q}
                        onChange={value => handleChange(q.id, value)}
                        value={feedback.answers[q.id]}
                    />
                ))}
                <div className="control-section">
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={this.handleSubmit}
                        type="submit"
                    >
                        {isLoading ? 'Saving...' : 'Submit'}
                    </Button>
                    <Link to="/">
                        <Button variant="contained" type="button">
                            Cancel
                        </Button>
                    </Link>
                </div>
            </form>
        );
    }
}

QuizViewForm.propTypes = {
    feedback: PropTypes.shape({
        quizId: PropTypes.string.isRequired,
        answers: PropTypes.shape({}),
    }).isRequired,
    handleChange: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    error: PropTypes.string.isRequired,
    quiz: PropTypes.shape({
        id: PropTypes.string,
        code: PropTypes.string,
        title: PropTypes.string,
        description: PropTypes.string,
        active: PropTypes.bool,
        questions: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number,
            text: PropTypes.string,
            type: PropTypes.string,
            answers: PropTypes.arrayOf(PropTypes.shape({
                id: PropTypes.number,
                text: PropTypes.string,
            })),
        })),
    }).isRequired,
};

export default QuizViewForm;
