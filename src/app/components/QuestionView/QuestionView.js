import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography/Typography';

import questionType from 'constants/questionType';
import {
    CheckAnswer,
    RadioAnswer,
    TextAnswer,
} from 'components';

import './questionView.scss';

class QuestionView extends React.Component {
    renderAnswers = () => {
        const { type, answers } = this.props.data;

        switch (type) {
        case questionType.SHORT: return (
            <TextAnswer
                onChange={this.props.onChange}
                value={this.props.value}
            />
        );
        case questionType.LONG: return (
            <TextAnswer
                multiLine
                onChange={this.props.onChange}
                value={this.props.value}
            />
        );
        case questionType.RADIO: return (
            <RadioAnswer
                answers={answers}
                onChange={this.props.onChange}
                value={this.props.value}
            />
        );
        case questionType.CHECK: return (
            <CheckAnswer
                answers={answers}
                onChange={this.props.onChange}
                value={this.props.value}
            />
        );
        default: return null;
        }
    };

    render() {
        const { text, answers } = this.props.data;

        return (
            <div className="question-view">
                <Typography variant="subtitle1" color="inherit">
                    {text}
                </Typography>
                {this.renderAnswers(answers)}
            </div>
        );
    }
}

QuestionView.propTypes = {
    data: PropTypes.shape({
        text: PropTypes.string,
        type: PropTypes.string,
        answers: PropTypes.arrayOf(PropTypes.shape({
            id: PropTypes.number,
            text: PropTypes.string,
        })),
    }).isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.arrayOf(PropTypes.string),
    ]).isRequired,
};

export default QuestionView;
