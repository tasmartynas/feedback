import React from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField/TextField';
import Button from '@material-ui/core/Button/Button';
import { Link } from 'react-router-dom';

import './quizCodeForm.scss';

class QuizCodeForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            code: '',
        };
    }

    handleChange = (e) => {
        const code = e.target.value.trim();

        if (code !== this.state.code) {
            this.setState({
                code,
            });
        }
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.submitForm(this.state.code);
    };

    isDisabled() {
        const { isLoading } = this.props;
        const { code } = this.state;

        return code.length <= 3 || isLoading;
    }

    render() {
        const { isLoading, error } = this.props;

        return (
            <form className="quiz-code-form" noValidate onSubmit={this.handleSubmit}>
                <div className="quiz-code">
                    <TextField
                        error={!!error}
                        label={error || 'Quiz Code'}
                        margin="normal"
                        onChange={this.handleChange}
                        required
                    />
                </div>
                <Link to="/quiz">
                    <Button
                        disabled={this.isDisabled()}
                        variant="contained"
                        color="primary"
                        size="large"
                        type="submit"
                    >
                        {isLoading ? 'Loading...' : 'Start'}
                    </Button>
                </Link>
            </form>
        );
    }
}

QuizCodeForm.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    submitForm: PropTypes.func.isRequired,
    error: PropTypes.string.isRequired,
};

export default QuizCodeForm;
