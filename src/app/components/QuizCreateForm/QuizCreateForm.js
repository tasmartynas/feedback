import React from 'react';
import PropTypes from 'prop-types';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField/TextField';

import { Questions } from 'components';

import './quizCreateForm.scss';

class QuizCreateForm extends React.Component {
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.handleSubmit();
    };

    render() {
        return (
            <form className="quiz-create-form" noValidate onSubmit={this.handleSubmit}>
                <TextField
                    name="title"
                    label="Quiz Title"
                    placeholder="Quiz Title"
                    fullWidth
                    autoComplete="off"
                    margin="normal"
                    onChange={this.props.changeField}
                />
                <TextField
                    name="description"
                    label="Quiz Description"
                    placeholder="Quiz Description"
                    fullWidth
                    autoComplete="off"
                    margin="normal"
                    onChange={this.props.changeField}
                />
                <TextField
                    name="code"
                    label="Quiz Code"
                    placeholder="Quiz Code"
                    fullWidth
                    autoComplete="off"
                    margin="normal"
                    onChange={this.props.changeField}
                />
                <Questions
                    questions={this.props.questions}
                    changeQuestion={this.props.changeQuestion}
                    removeQuestion={this.props.removeQuestion}
                />
                <div className="control-section">
                    <Button
                        variant="contained"
                        color="primary"
                        type="submit"
                    >
                        Submit
                    </Button>
                    <Button variant="contained" type="button">
                        Cancel
                    </Button>
                </div>
            </form>
        );
    }
}

QuizCreateForm.propTypes = {
    questions: PropTypes.arrayOf(
        PropTypes.shape({
            id: PropTypes.number.isRequired,
            type: PropTypes.string.isRequired,
        }),
    ).isRequired,
    changeField: PropTypes.func.isRequired,
    changeQuestion: PropTypes.func.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    removeQuestion: PropTypes.func.isRequired,
};

export default QuizCreateForm;
