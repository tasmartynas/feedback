import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';

import QUESTION_TYPE from 'constants/questionType';

const styles = {
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
};

class Header extends React.Component {
    state = {
        anchorEl: null,
    };

    handleAdd = (type) => {
        this.props.addQuestion(type);
        this.handleClose();
    };

    handleMenu = (event) => {
        this.setState({ anchorEl: event.currentTarget });
    };

    handleClose = () => {
        this.setState({ anchorEl: null });
    };

    render() {
        const { addQuestion, classes, title } = this.props;
        const { anchorEl } = this.state;
        const open = Boolean(anchorEl);

        return (
            <div className={classes.root}>
                <AppBar position="static">
                    <Toolbar>
                        <Typography variant="h6" color="inherit" className={classes.grow}>
                            { title }
                        </Typography>
                        {addQuestion && (
                            <div>
                                <Button
                                    aria-owns={open ? 'menu-appbar' : null}
                                    aria-haspopup="true"
                                    color="inherit"
                                    onClick={this.handleMenu}
                                >
                                    Add Question
                                </Button>
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={anchorEl}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={open}
                                    onClose={this.handleClose}
                                >
                                    <MenuItem
                                        onClick={() => this.handleAdd(QUESTION_TYPE.RADIO)}
                                    >
                                        Radio
                                    </MenuItem>
                                    <MenuItem
                                        onClick={() => this.handleAdd(QUESTION_TYPE.CHECK)}
                                    >
                                        Checkbox
                                    </MenuItem>
                                    <MenuItem
                                        onClick={() => this.handleAdd(QUESTION_TYPE.SHORT)}
                                    >
                                        Short Text
                                    </MenuItem>
                                    <MenuItem
                                        onClick={() => this.handleAdd(QUESTION_TYPE.LONG)}
                                    >
                                        Long Text
                                    </MenuItem>
                                </Menu>
                            </div>
                        )}
                    </Toolbar>
                </AppBar>
            </div>
        );
    }
}

Header.propTypes = {
    addQuestion: PropTypes.func,
    classes: PropTypes.shape({}).isRequired,
    title: PropTypes.string.isRequired,
};

Header.defaultProps = {
    addQuestion: undefined,
};

export default withStyles(styles)(Header);
